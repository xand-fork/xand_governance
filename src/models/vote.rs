#[derive(Debug, Eq, PartialEq)]
pub enum VoteDirection {
    Approve,
    Reject,
}

#[derive(Debug, Eq, PartialEq)]
pub struct VoteTally {
    pub yea_votes: usize,
    pub nay_votes: usize,
    pub total_participant_count: usize,
}

#[derive(Debug, Eq, PartialEq)]
pub struct VoteTallies {
    pub member_tally: VoteTally,
    pub validator_tally: VoteTally,
}
