#[derive(Debug, Eq, PartialEq)]
pub enum Proposal {
    AddMember,
    RemoveMember,
    AddValidator,
    RemoveValidator,
    SetTrust,
    SetLimitedAgent,
    AddCidrBlock,
    RemoveCidrBlock,
    SetValidatorEmissionRate,
    RuntimeUpgrade,
}

#[derive(Debug, Eq, PartialEq)]
pub enum ProposalStatus {
    Open,
    Accepted,
    Rejected,
}

#[cfg(test)]
mod proposal_test_extensions {
    use super::{
        Proposal,
        Proposal::{
            AddCidrBlock, AddMember, AddValidator, RemoveCidrBlock, RemoveMember, RemoveValidator,
            RuntimeUpgrade, SetLimitedAgent, SetTrust, SetValidatorEmissionRate,
        },
    };

    impl Proposal {
        pub fn all() -> [Self; 10] {
            [
                AddMember,
                RemoveMember,
                AddValidator,
                RemoveValidator,
                SetTrust,
                SetLimitedAgent,
                AddCidrBlock,
                RemoveCidrBlock,
                SetValidatorEmissionRate,
                RuntimeUpgrade,
            ]
        }
    }
}
