mod add_cidr_block;
mod add_member;
mod add_validator;
mod remove_cidr_block;
mod remove_member;
mod remove_validator;
mod runtime_upgrade;
mod set_limited_agent;
mod set_trust;
mod set_validator_emission_rate;

pub mod templates {
    use crate::models::proposal::{Proposal, ProposalStatus};
    use crate::models::vote::{VoteTallies, VoteTally};
    use crate::ports::status::XandProposalStatusCheck;
    use crate::proposal_status::XandProposalStatusCheckImpl;

    pub fn assert_member_only_majority_yea_vote_is_accepted(proposal: &Proposal) {
        // Given
        let member_tally = VoteTally {
            yea_votes: 51,
            nay_votes: 4,
            total_participant_count: 100,
        };
        let validator_tally = VoteTally {
            yea_votes: 0,
            nay_votes: 0,
            total_participant_count: 4,
        };
        let tallies = VoteTallies {
            member_tally,
            validator_tally,
        };

        // When
        let status = XandProposalStatusCheckImpl::check_status(proposal, &tallies);

        // Then
        assert_eq!(status, ProposalStatus::Accepted);
    }

    pub fn assert_member_only_majority_yea_vote_with_unexpected_validator_votes_is_accepted(
        proposal: &Proposal,
    ) {
        // Given 51/100 yea votes (100% member weighting)
        let member_tally = VoteTally {
            yea_votes: 51,
            nay_votes: 0,
            total_participant_count: 100,
        };
        let validator_tally = VoteTally {
            yea_votes: 10,
            nay_votes: 100,
            total_participant_count: 100,
        };
        let tallies = VoteTallies {
            member_tally,
            validator_tally,
        };

        // When
        let status = XandProposalStatusCheckImpl::check_status(proposal, &tallies);

        // Then
        assert_eq!(status, ProposalStatus::Accepted);
    }

    pub fn assert_member_only_50_pct_nay_vote_prevents_majority_and_rejects(proposal: &Proposal) {
        // Given 50/100 nay votes (100% member weighting)
        let member_tally = VoteTally {
            yea_votes: 1,
            nay_votes: 50,
            total_participant_count: 100,
        };
        let validator_tally = VoteTally {
            yea_votes: 1,
            nay_votes: 2,
            total_participant_count: 1000,
        };
        let tallies = VoteTallies {
            member_tally,
            validator_tally,
        };

        // When
        let status = XandProposalStatusCheckImpl::check_status(proposal, &tallies);

        // Then the proposal has failed
        assert_eq!(status, ProposalStatus::Rejected);
    }

    pub fn assert_member_only_40_pct_yea_majority_remains_open(proposal: &Proposal) {
        // Given
        let member_tally = VoteTally {
            yea_votes: 40,
            nay_votes: 0,
            total_participant_count: 100,
        };
        let validator_tally = VoteTally {
            yea_votes: 0,
            nay_votes: 0,
            total_participant_count: 4,
        };
        let tallies = VoteTallies {
            member_tally,
            validator_tally,
        };

        // When
        let status = XandProposalStatusCheckImpl::check_status(proposal, &tallies);

        // Then
        assert_eq!(status, ProposalStatus::Open);
    }

    pub fn assert_member_only_40_pct_nay_majority_remains_open(proposal: &Proposal) {
        // Given
        let member_tally = VoteTally {
            yea_votes: 0,
            nay_votes: 40,
            total_participant_count: 100,
        };
        let validator_tally = VoteTally {
            yea_votes: 0,
            nay_votes: 0,
            total_participant_count: 4,
        };
        let tallies = VoteTallies {
            member_tally,
            validator_tally,
        };

        // When
        let status = XandProposalStatusCheckImpl::check_status(proposal, &tallies);

        // Then
        assert_eq!(status, ProposalStatus::Open);
    }

    pub fn assert_member_only_yea_votes_majority_threshold_exactly_remains_open(
        proposal: &Proposal,
    ) {
        // Given - 2/4 members (50%) at exactly threshold
        let member_tally = VoteTally {
            yea_votes: 2,
            nay_votes: 0,
            total_participant_count: 4,
        };
        let validator_tally = VoteTally {
            yea_votes: 0,
            nay_votes: 0,
            total_participant_count: 100,
        };
        let tallies = VoteTallies {
            member_tally,
            validator_tally,
        };

        // When
        let status = XandProposalStatusCheckImpl::check_status(proposal, &tallies);

        // Then
        assert_eq!(status, ProposalStatus::Open);
    }

    pub fn assert_49_51_split_yea_votes_supermajority_threshold_exactly_remains_open(
        proposal: &Proposal,
    ) {
        // Given 100/147 * 49/100  +  100/153 * 51/100  == 2/3
        let member_tally = VoteTally {
            yea_votes: 100,
            nay_votes: 0,
            total_participant_count: 147,
        };
        let validator_tally = VoteTally {
            yea_votes: 100,
            nay_votes: 0,
            total_participant_count: 153,
        };
        let tallies = VoteTallies {
            member_tally,
            validator_tally,
        };

        // When
        let status = XandProposalStatusCheckImpl::check_status(proposal, &tallies);

        // Then
        assert_eq!(status, ProposalStatus::Open);
    }

    pub fn assert_49_51_split_over_yea_supermajority_is_accepted(proposal: &Proposal) {
        // Given 100/147 * 49/100  +  101/153 * 51/100  > 2/3
        let member_tally = VoteTally {
            yea_votes: 100,
            nay_votes: 0,
            total_participant_count: 147,
        };
        let validator_tally = VoteTally {
            yea_votes: 101,
            nay_votes: 0,
            total_participant_count: 153,
        };
        let tallies = VoteTallies {
            member_tally,
            validator_tally,
        };

        // When
        let status = XandProposalStatusCheckImpl::check_status(proposal, &tallies);

        // Then
        assert_eq!(status, ProposalStatus::Accepted);
    }

    pub fn assert_49_51_split_under_yea_supermajority_remains_open(proposal: &Proposal) {
        // Given 99/147 * 49/100  +  100/153 * 51/100  < 2/3
        let member_tally = VoteTally {
            yea_votes: 99,
            nay_votes: 0,
            total_participant_count: 147,
        };
        let validator_tally = VoteTally {
            yea_votes: 100,
            nay_votes: 0,
            total_participant_count: 153,
        };
        let tallies = VoteTallies {
            member_tally,
            validator_tally,
        };

        // When
        let status = XandProposalStatusCheckImpl::check_status(proposal, &tallies);

        // Then the status is Open
        assert_eq!(status, ProposalStatus::Open);
    }

    pub fn assert_49_51_split_over_yea_supermajority_with_no_members_is_accepted(
        proposal: &Proposal,
    ) {
        // Given 21/30 > 2/3 when no members are in the network
        let member_tally = VoteTally {
            yea_votes: 0,
            nay_votes: 0,
            total_participant_count: 0,
        };
        let validator_tally = VoteTally {
            yea_votes: 21,
            nay_votes: 0,
            total_participant_count: 30,
        };
        let tallies = VoteTallies {
            member_tally,
            validator_tally,
        };

        // When
        let status = XandProposalStatusCheckImpl::check_status(proposal, &tallies);

        // Then
        assert_eq!(status, ProposalStatus::Accepted);
    }

    pub fn assert_49_51_split_voting_nay_preventing_supermajority_is_rejected(proposal: &Proposal) {
        // Given 47/147 * 49/100  +  53/153 * 51/100  == 1/3  (Nay votes threshold prevention)
        let member_tally = VoteTally {
            yea_votes: 0,
            nay_votes: 47,
            total_participant_count: 147,
        };
        let validator_tally = VoteTally {
            yea_votes: 0,
            nay_votes: 53,
            total_participant_count: 153,
        };
        let tallies = VoteTallies {
            member_tally,
            validator_tally,
        };

        // When
        let status = XandProposalStatusCheckImpl::check_status(proposal, &tallies);

        // Then
        assert_eq!(status, ProposalStatus::Rejected);
    }

    pub fn assert_49_51_split_voting_nay_preventing_supermajority_with_no_member_voters_is_rejected(
        proposal: &Proposal,
    ) {
        // Given (10/30) == 1/3 (Nay votes preventing threshold)
        let member_tally = VoteTally {
            yea_votes: 0,
            nay_votes: 0,
            total_participant_count: 0,
        };
        let validator_tally = VoteTally {
            yea_votes: 0,
            nay_votes: 10,
            total_participant_count: 30,
        };
        let tallies = VoteTallies {
            member_tally,
            validator_tally,
        };

        // When
        let status = XandProposalStatusCheckImpl::check_status(proposal, &tallies);

        // Then
        assert_eq!(status, ProposalStatus::Rejected);
    }
}
