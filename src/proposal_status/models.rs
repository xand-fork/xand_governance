#[derive(Debug)]
pub struct Ratio {
    pub numerator: usize,
    pub denominator: usize,
}
impl Ratio {
    fn new(numerator: usize, denominator: usize) -> Self {
        Self {
            numerator,
            denominator,
        }
    }
}

/// Threshold of Yea votes required to be surpassed to consider a proposal as accepted
#[derive(Debug, Eq, PartialEq)]
pub enum RequiredThreshold {
    /// Supermajority requires > 2/3 Yea votes to be accepted
    Supermajority,
    /// Majority requires > 1/2 Yea votes to be accepted
    Majority,
}
impl RequiredThreshold {
    const SUPERMAJORITY_RATIO: Ratio = Ratio {
        numerator: 2,
        denominator: 3,
    };

    const MAJORITY_RATIO: Ratio = Ratio {
        numerator: 1,
        denominator: 2,
    };

    /// Returns the threshold expressed as a fraction
    pub fn ratio(&self) -> &Ratio {
        match self {
            RequiredThreshold::Supermajority => &Self::SUPERMAJORITY_RATIO,
            RequiredThreshold::Majority => &Self::MAJORITY_RATIO,
        }
    }

    /// Used to derive the ratio of nay votes that would make a proposal Rejected
    pub fn failure_numerator(&self) -> usize {
        self.ratio().denominator - self.ratio().numerator
    }
}

/// Expresses the weighting ratios of votes specifically for Member and Validator roles (only roles that can currently vote)
/// Total of all ratios should equal 100%
#[derive(Debug)]
pub struct VoteWeights {
    /// The weight a member vote holds relative to all votes
    member_weight: Ratio,
    /// The weight a validator vote holds relative to all votes
    validator_weight: Ratio,
}
impl VoteWeights {
    pub fn member_weight(&self) -> &Ratio {
        &self.member_weight
    }

    pub fn validator_weight(&self) -> &Ratio {
        &self.validator_weight
    }

    /// Returns a VoteWeights structure with 49% Member weighting and 51% Validator weighting
    pub fn split_49_51() -> Self {
        Self {
            member_weight: Ratio::new(49, 100),
            validator_weight: Ratio::new(51, 100),
        }
    }

    /// Returns a VoteWeights structure with 100% Member weighting and 0% Validator weighting
    pub fn full_member_weight() -> Self {
        Self {
            member_weight: Ratio::new(100, 100),
            validator_weight: Ratio::new(0, 100),
        }
    }
}
