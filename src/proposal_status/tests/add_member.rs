use crate::proposal_status::tests::templates::assert_49_51_split_yea_votes_supermajority_threshold_exactly_remains_open;
use crate::{
    models::proposal::Proposal,
    proposal_status::tests::templates::{
        assert_49_51_split_over_yea_supermajority_is_accepted,
        assert_49_51_split_over_yea_supermajority_with_no_members_is_accepted,
        assert_49_51_split_under_yea_supermajority_remains_open,
        assert_49_51_split_voting_nay_preventing_supermajority_is_rejected,
        assert_49_51_split_voting_nay_preventing_supermajority_with_no_member_voters_is_rejected,
    },
};

#[test]
fn check_status__members_and_validators_vote_yea_supermajority_threshold__accepted() {
    assert_49_51_split_over_yea_supermajority_is_accepted(&Proposal::AddMember)
}

#[test]
fn check_status__members_and_validators_vote_yea_under_supermajority_threshold__open() {
    assert_49_51_split_under_yea_supermajority_remains_open(&Proposal::AddMember)
}

#[test]
fn check_status__validators_can_reach_supermajority_when_no_members_exist__accepted() {
    assert_49_51_split_over_yea_supermajority_with_no_members_is_accepted(&Proposal::AddMember)
}

#[test]
fn check_status__members_and_validators_vote_nay_to_prevent_supermajority__rejected() {
    assert_49_51_split_voting_nay_preventing_supermajority_is_rejected(&Proposal::AddMember)
}

#[test]
fn check_status__validators_can_vote_nay_to_prevent_supermajority_when_no_members_on_chain__rejected(
) {
    assert_49_51_split_voting_nay_preventing_supermajority_with_no_member_voters_is_rejected(
        &Proposal::AddMember,
    )
}

#[test]
fn check_status__members_and_validators_vote_yea_exact_supermajority_threshold__open() {
    assert_49_51_split_yea_votes_supermajority_threshold_exactly_remains_open(&Proposal::AddMember)
}
