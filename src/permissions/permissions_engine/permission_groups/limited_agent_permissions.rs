use crate::models::proposal::Proposal;
use crate::permissions::permissions_engine::permission_groups::port::Permissions;

pub struct LimitedAgentPermissions {}

impl Permissions for LimitedAgentPermissions {
    fn can_vote(&self, _proposal: &Proposal) -> bool {
        false
    }

    fn can_propose(&self, proposal: &Proposal) -> bool {
        match proposal {
            Proposal::AddMember => true,
            Proposal::RemoveMember => true,
            Proposal::AddValidator => true,
            Proposal::RemoveValidator => true,
            Proposal::SetTrust => true,
            Proposal::SetLimitedAgent => true,
            Proposal::AddCidrBlock => true,
            Proposal::RemoveCidrBlock => true,
            Proposal::SetValidatorEmissionRate => true,
            Proposal::RuntimeUpgrade => true,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn can_propose__all_proposals_returns_true() {
        // Given
        let proposals = Proposal::all();
        let perms = LimitedAgentPermissions {};

        for p in proposals.iter() {
            // When
            let res = perms.can_propose(p);

            // Then
            assert!(res, "Assertion failed for: {:?}", p);
        }
    }

    #[test]
    fn can_vote__all_proposals_return_false() {
        // Given
        let proposals = Proposal::all();
        let perms = LimitedAgentPermissions {};

        for p in proposals.iter() {
            // When
            let res = perms.can_vote(p);

            // Then
            assert!(!res, "Assertion failed for: {:?}", p);
        }
    }
}
