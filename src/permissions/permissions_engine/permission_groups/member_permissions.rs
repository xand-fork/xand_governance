use crate::models::proposal::Proposal;
use crate::permissions::permissions_engine::permission_groups::port::Permissions;

pub struct MemberPermissions {}

impl Permissions for MemberPermissions {
    fn can_vote(&self, _proposal: &Proposal) -> bool {
        true
    }

    fn can_propose(&self, _proposal: &Proposal) -> bool {
        true
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn can_propose__all_proposals_returns_true() {
        // Given
        let proposals = Proposal::all();
        let perms = MemberPermissions {};

        for p in proposals.iter() {
            // When
            let res = perms.can_propose(p);

            // Then
            assert!(res, "Assertion failed for: {:?}", p);
        }
    }

    #[test]
    fn can_vote__all_proposals_returns_true() {
        // Given
        let proposals = Proposal::all();
        let perms = MemberPermissions {};

        for p in proposals.iter() {
            // When
            let res = perms.can_vote(p);

            // Then
            assert!(res, "Assertion failed for: {:?}", p);
        }
    }
}
