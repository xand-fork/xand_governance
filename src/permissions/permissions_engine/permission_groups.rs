/// Permissions groups conveniently map 1:1 to Role.
pub mod limited_agent_permissions;
pub mod member_permissions;
pub mod validator_permissions;

pub mod port {
    // For automock generated types
    #![allow(clippy::upper_case_acronyms)]

    use crate::models::proposal::Proposal;
    #[cfg(test)]
    use mockall::{automock, predicate::*};

    /// If implementing a new permissions group, you must define these methods
    /// for a given Proposal
    #[cfg_attr(test, automock)]
    pub trait Permissions {
        fn can_vote(&self, proposal: &Proposal) -> bool;
        fn can_propose(&self, proposal: &Proposal) -> bool;
    }
}
