use crate::models::proposal::Proposal;
use crate::models::role::Role;

pub trait XandGovernancePermissions {
    fn can_propose(role: Role, proposal: Proposal) -> bool;
    fn can_vote(role: Role, proposal: Proposal) -> bool;
}
