# Xand Governance

This crate defines our domain models and logic for the xand governance. IE: This is the code
 that defines Proposals and their relevant rules and permissions (voting, propsoing, tallying).
 It is free of major dependencies and should remain so to provide a clean
 environment for concentrating on domain code.

 Practically speaking this code is consumed in other places for two purposes:
 * Evaluting permissions for different roles to vote/propose (within the substrate runtime)
 * Logic for tallying votes on Proposals (within the substrate runtime)

 The crate has an `std` feature that is enabled by default. Disabling the feature allows the
 crate to compile in no-std mode. The substrate runtime requires this, and no-std support must
 be maintained.

